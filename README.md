# Everli Parser

Parse virtual receipt from the online groceries service "Everli" into a CSV file.

## Usage

Save the receipt HTML page from your browser as an html file and then:

```bash
node src/index.js <filaneme>
```
