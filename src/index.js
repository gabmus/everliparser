import { JSDOM } from "jsdom";
import { convert } from "html-to-text";
import fs from "fs";

const ANY_NEWLINES_REGEX = new RegExp("\n+");

function convertOneline(html) {
    return convert(html).replace(ANY_NEWLINES_REGEX, " ").replace('"', "'");
}

function parsePrice(p) {
    return parseFloat(p.replace("€", "").replace(",", ".").trim());
}

function removeNonDiscount(el) {
    for (const toDel of el.getElementsByClassName("strikethrough")) {
        toDel.remove()
    }
    return el
}

function parse(filename) {
    const docStr = fs.readFileSync(fs.openSync(filename));
    const dom = new JSDOM(docStr);
    const d = dom.window.document;

    const data = Array.from(d.getElementsByClassName("vader-cart-item")).map(
        (ci) => [
            convertOneline(ci.getElementsByClassName("item-name")[0].innerHTML),
            parsePrice(convertOneline(
                removeNonDiscount(ci.getElementsByClassName("item-price")[0]).innerHTML
            )),
            convertOneline(ci.getElementsByClassName("item-quantity")[0].innerHTML),
        ],
    );

    const csvLines = data.map(([name, price, quantity]) => `"${name}";"${quantity}";"${price}"`)

    console.log(csvLines.join('\n'))
}

const filename = process.argv[2]

if (!filename) {
    console.log(`Usage: ${process.argv[0]} ${process.argv[1]} <filename>`)
    process.exit(1)
}

parse(filename);
